# Object Oriented Programming in C# 
Build on the fundamentals that were covered in Introduction to C#.  You will extend your knowledge by applying core OOP principles to the code and applications you will create in this course. You will build a  knowledge of encapsulation, inheritance and polymorphism.   You will also learn memory management in the .NET framework.

What you'll learn
Core object-oriented programming concepts
How to create and use classes and objects in a C# application
Applying the three core OOP concepts using C#
A grasp of memory and resource management in C# and the .NET Framework